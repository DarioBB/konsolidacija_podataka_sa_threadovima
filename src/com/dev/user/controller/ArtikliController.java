package com.dev.user.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.dev.user.model.Artikli;

public class ArtikliController {

	public List<Artikli> getFileValues(String filename)
	{
		BufferedReader in = null;
		InputStream file = null;
		try {
			file = new FileInputStream(new File(filename));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			in = new BufferedReader(new InputStreamReader(file, "Windows-1250"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<Artikli> lista_artikli = new ArrayList<Artikli>();
		
        String inputLine;
        String[] parts = null;
        try {
			while ((inputLine = in.readLine()) != null) 
			{
		        //System.out.println(inputLine+"");
		    	try{
		    		parts = inputLine.split("\\|");
	    			
	    			lista_artikli.add(new Artikli(parts[0],parts[1],parts[2]));
	    			
		    	} catch (java.lang.NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return lista_artikli;
	}
	
}
