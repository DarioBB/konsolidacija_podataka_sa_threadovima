package com.dev.user.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class CjenikController {
	
	public double getPrice(String filename, String sifra)
	{
		BufferedReader in = null;
		InputStream file = null;
		try {
			file = new FileInputStream(new File(filename));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			in = new BufferedReader(new InputStreamReader(file, "Windows-1250"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		NumberFormat format = NumberFormat.getInstance(Locale.ITALIAN);
		Number number = null;
		double price = 0;
        String inputLine;
        String[] parts = null;
        try {
			while ((inputLine = in.readLine()) != null) 
			{
		        //System.out.println(inputLine+"");
				if(inputLine.contains(sifra))
				{
			    	try{
			    		parts = inputLine.split("\\|");
			    		//price = parts[1];
			    		number = format.parse(parts[1]);
			    	    price = number.doubleValue();
		    			
			    	} catch (java.lang.NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			    	break;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		return price;
	}
}
