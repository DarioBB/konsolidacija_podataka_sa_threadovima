package com.dev.user.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TecajnaListaController {

	public String[] getParamValues(String parametri_txt)
	{
		String[] data = new String[2];
		
		BufferedReader in = null;
		InputStream file = null;
		try {
			file = new FileInputStream(new File(parametri_txt));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			//e3.printStackTrace();
			System.out.println("Datoteka "+parametri_txt+" nije pronađena, molimo unesite pravilnu putanju do datoteke");
		}
		in = new BufferedReader(new InputStreamReader(file));
		
        String inputLine;
        String[] parts = null;
        String txt_sifra = null;
        String txt_datum = null;
        try {
			while ((inputLine = in.readLine()) != null) 
			{
		        //System.out.println(inputLine+"");
		    	try{
		    		parts = inputLine.split("\\|");
		    		
		    		txt_sifra = parts[0];
		    		txt_datum = parts[1];
	    			System.out.println(""+parametri_txt+" - Šifra: "+txt_sifra+", datum: "+txt_datum);
		    	} catch (java.lang.NullPointerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        data[0] = txt_sifra;
        data[1] = txt_datum;
        
		return data;
	}
	
	public double getRatesData(String sifra, String datum_konverzije) {
		
		Date dat = null;
		String datum_konverzije_new = null;
		try {
			dat = new SimpleDateFormat("dd/MM/yyyy").parse(datum_konverzije);
			datum_konverzije_new = new SimpleDateFormat("ddMMyy").format(dat);
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		URL oracle = null;
		
		BufferedReader in = null;
		
		String inputLine;
        String[] parts = null;
        String url_sifra = null;
        String url_srednji_tecaj = null;
        
        NumberFormat format = NumberFormat.getInstance(Locale.ITALIAN);
		Number number = null;
		double srednji_tecaj = 0;
		try {
			oracle = new URL("http://www.hnb.hr/tecajn/f"+datum_konverzije_new+".dat");
			
			HttpURLConnection huc = (HttpURLConnection) oracle.openConnection();
			huc.setRequestMethod("HEAD");
			boolean resp = (huc.getResponseCode() == HttpURLConnection.HTTP_OK);
			if(resp == false)
			{
				Calendar cal = Calendar.getInstance();
				cal.setTime(dat);
				cal.add(Calendar.DATE, -1);
				Date dateBeforeDays = cal.getTime();
				datum_konverzije = new SimpleDateFormat("dd/MM/yy").format(dateBeforeDays);
				
				return this.getRatesData(sifra, datum_konverzije); // rekurzivni poziv, zovi dok se ne naide na datum za koji postoji url izvor
				//return 1; // da bi se prekinulo izvrsavanje ostatka koda i izaslo iz metode
			}
			else 
			{
				System.out.println("Datum navedene ili zadnje postojeće tečajne liste: "+datum_konverzije+"");
		        URLConnection yc = null;
				try {
					yc = oracle.openConnection();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        in = null;
				try {
					in = new BufferedReader(new InputStreamReader(
					                        yc.getInputStream()));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		        //String inputLine;
		        int j = 0;
		        //int k = 0;
		        try {
					while ((inputLine = in.readLine()) != null) 
					{
						//String[] parts = null;
						if(j > 0)
						{
					        //System.out.println(inputLine+"");
					    	try{
					    		parts = inputLine.split("      ");
					    		
					    		if(parts[0].trim().substring(0, 3).equals(sifra))
					    		{
					    			url_sifra = parts[0].trim().substring(0, 3);
					    			url_srednji_tecaj = parts[2].trim();
					    			
					    			number = format.parse(parts[2].trim());
					    			srednji_tecaj = number.doubleValue();
					    			//System.out.println("Šifra: "+url_sifra+", srednji tečaj: "+url_srednji_tecaj);
					    		}
					    	} catch (java.lang.NullPointerException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					    	catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
					    	//k++;
						}
						j++;
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
        
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Šifra: "+url_sifra+", srednji tečaj: "+url_srednji_tecaj);
        
		return srednji_tecaj;
		//return true;
	}
	
}
