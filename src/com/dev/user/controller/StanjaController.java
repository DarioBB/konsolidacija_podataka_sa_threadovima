package com.dev.user.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class StanjaController {

	public double[] getAmountInAllStores(String filename, String sifra)
	{
		BufferedReader in = null;
		InputStream file = null;
		try {
			file = new FileInputStream(new File(filename));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			in = new BufferedReader(new InputStreamReader(file, "Windows-1250"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		NumberFormat format = NumberFormat.getInstance(Locale.ITALIAN);
		Number number = null;
		double[] results = new double[2];
		double amount = 0;
		double stores_count = 0;
        String inputLine;
        String[] parts = null;
        try {
			while ((inputLine = in.readLine()) != null) 
			{
		        //System.out.println(inputLine+"");
				if(inputLine.contains(sifra))
				{
			    	try{
			    		parts = inputLine.split("\\|");
			    		
			    		number = format.parse(parts[2]);
			    	    amount += number.doubleValue();
			    	    stores_count++;
			    	} catch (java.lang.NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        results[0] = amount;
        results[1] = stores_count;

        return results;
	}
	
	public double[] getPricesInAllStores(String filename, String sifra)
	{
		CjenikController cc = new CjenikController();
		
		BufferedReader in = null;
		InputStream file = null;
		try {
			file = new FileInputStream(new File(filename));
		} catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		try {
			in = new BufferedReader(new InputStreamReader(file, "Windows-1250"));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		NumberFormat format = NumberFormat.getInstance(Locale.ITALIAN);
		Number number = null;
		double[] results = new double[2];
		double amount = 0;
		double article_on_store_count = 0;
        String inputLine;
        String[] parts = null;
        try {
			while ((inputLine = in.readLine()) != null) 
			{
		        //System.out.println(inputLine+"");
				if(inputLine.contains("|"+sifra+"|"))
				{
			    	try{
			    		parts = inputLine.split("\\|");
			    		
			    		double cijena_art_kn = cc.getPrice("resources/cjenik.txt", parts[0]);
			    		number = format.parse(parts[2]);
			    	    amount += number.doubleValue() * cijena_art_kn;
			    	    article_on_store_count++;
			    	} catch (java.lang.NullPointerException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        results[0] = amount;
        results[1] = article_on_store_count;

        return results;
	}
	
}
