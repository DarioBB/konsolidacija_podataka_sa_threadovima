package com.dev.user.app;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import com.dev.user.controller.ArtikliController;
import com.dev.user.controller.CjenikController;
import com.dev.user.controller.ProdajnaMjestaController;
import com.dev.user.controller.SpremanjeController;
import com.dev.user.controller.StanjaController;
import com.dev.user.controller.TecajnaListaController;
import com.dev.user.model.Artikli;
import com.dev.user.model.ProdajnaMjesta;

public class App {
	
	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		
		final SpremanjeController spremanje = new SpremanjeController();

		String parametri_txt = "parametri.txt";
		final ArtikliController ac = new ArtikliController();
		final CjenikController cc = new CjenikController();
		final ProdajnaMjestaController pmc = new ProdajnaMjestaController();
		final StanjaController sc = new StanjaController();
		
		/* pocetak koda - dohvacanje podataka sa tecajne liste preko definirane txt datoteke */
		TecajnaListaController tlc = new TecajnaListaController();
		
		String[] param_values = tlc.getParamValues(parametri_txt);
		
		String sifra = param_values[0]; 
		String datum_konverzije = param_values[1];
		
		final double srednji_tecaj = tlc.getRatesData(sifra, datum_konverzije);
		/* kraj koda - dohvacanje podataka sa tecajne liste preko definirane txt datoteke */
		
		DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ITALIAN);
        otherSymbols.setDecimalSeparator(',');
        otherSymbols.setGroupingSeparator('.');
        final DecimalFormat df = new DecimalFormat("###,###.###", otherSymbols);
        
		Thread thread1 = new Thread() {
		    public void run() {
		    	
				final List<Artikli> lista_artikli = ac.getFileValues("resources/artikli.txt");
				Collections.sort(lista_artikli, new Comparator<Artikli>() {
				    public int compare(Artikli v1, Artikli v2) {
				        return v1.sifra.compareTo(v2.sifra);
				    }
				});
				
				String file_contents = "";
				String file1_data = "";
				for (int i = 0; i < lista_artikli.size(); i++) {
					
					double cijena_art_kn = cc.getPrice("resources/cjenik.txt", lista_artikli.get(i).sifra);
					double[] art_sve_prodavaonice = sc.getAmountInAllStores("resources/stanja.txt", lista_artikli.get(i).sifra);
					
					double uk_vrijednost_art_sve_prodavaonice_kn = cijena_art_kn * art_sve_prodavaonice[0];
					double uk_vrijednost_art_sve_prodavaonice_valuta = uk_vrijednost_art_sve_prodavaonice_kn / srednji_tecaj;
					
					file_contents = lista_artikli.get(i).sifra+"\t"+lista_artikli.get(i).naziv+"\t"+df.format(cijena_art_kn)+"\t"+df.format(art_sve_prodavaonice[0])+"\t"+lista_artikli.get(i).jedinica_mjere+"\t"+df.format(uk_vrijednost_art_sve_prodavaonice_kn)+"\t"+df.format(uk_vrijednost_art_sve_prodavaonice_valuta)+"\t"+df.format(art_sve_prodavaonice[1])+"\n";
					
					System.out.print(file_contents);
					file1_data += file_contents;
				}
				
				if(file1_data != "")
				{
					spremanje.saveFile("results/vrijednost zalihe - artikli.txt", file1_data);
				}
		    }
		};

		Thread thread2 = new Thread() {
		    public void run() {
		    	
				List<ProdajnaMjesta> pm = pmc.getFileValues("resources/pm.txt");
				Collections.sort(pm, new Comparator<ProdajnaMjesta>() {
				    public int compare(ProdajnaMjesta v1, ProdajnaMjesta v2) {
				        return v1.sifra.compareTo(v2.sifra);
				    }
				});
				
				String file_contents = "";
				String file2_data = "";
				for (int i = 0; i < pm.size(); i++) {
					double[] zal_art_sve_prodavaonice = sc.getPricesInAllStores("resources/stanja.txt", pm.get(i).sifra);
					double uk_vrijednost_zal_art_sve_prodavaonice_valuta = zal_art_sve_prodavaonice[0] / srednji_tecaj;
					
					file_contents = pm.get(i).sifra+"\t"+pm.get(i).naziv+"\t"+df.format(zal_art_sve_prodavaonice[0])+"\t"+df.format(uk_vrijednost_zal_art_sve_prodavaonice_valuta)+"\t"+df.format(zal_art_sve_prodavaonice[1])+"\n";
					
					System.out.print(file_contents);
					file2_data += file_contents;
				}
				
				if(file2_data != "")
				{
					spremanje.saveFile("results/vrijednost zalihe - PM.txt", file2_data);
					//file2_result = app.saveFile("results/vrijednost zalihe - PM.txt", file2_data);
				}
				
		    }
		};
		
		
		thread1.start();
		thread2.start();

		// Wait for them both to finish
		try {
			thread1.join();
			thread2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("\nKonsolidacijske datoteke su spremljene u results direktorij");
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		double seconds = totalTime / 1000.0;
		System.out.println("\nVrijeme izvršavanja programa: "+seconds+" sekundi");
	
	}

}
