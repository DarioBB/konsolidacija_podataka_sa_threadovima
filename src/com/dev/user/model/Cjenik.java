package com.dev.user.model;

public class Cjenik {
	
	public String sifra_artikla;
	public String cijena_artikla;
	
	public Cjenik(String sifra_artikla, String cijena_artikla){
        this.sifra_artikla = sifra_artikla;
        this.cijena_artikla = cijena_artikla;
    }
}
