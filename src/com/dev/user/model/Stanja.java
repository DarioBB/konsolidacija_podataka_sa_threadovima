package com.dev.user.model;

public class Stanja {
	
	public String sifra_artikla;
	public String sifra_prodajnog_mjesta;
	public String kolicina_artikla;
	
	public Stanja(String sifra_artikla, String sifra_prodajnog_mjesta, String kolicina_artikla){
        this.sifra_artikla = sifra_artikla;
        this.sifra_prodajnog_mjesta = sifra_prodajnog_mjesta;
        this.kolicina_artikla = kolicina_artikla;
    }
}
