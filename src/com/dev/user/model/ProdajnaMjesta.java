package com.dev.user.model;

public class ProdajnaMjesta {
	
	public String sifra;
	public String naziv;
	
	public ProdajnaMjesta(String sifra, String naziv){
        this.sifra = sifra;
        this.naziv = naziv;
    }
	
}
