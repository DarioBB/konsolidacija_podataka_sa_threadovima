Java aplikacija koja sa HNB-a vuče tečajnu listu za željeni datum te generira konsolidacijske podatke iz skladišta podataka. 
Iz 4 ulazne datoteke, formiraju se 2 izlazne datoteke. 
Korišteni su thread-ovi, tako da se je vrijeme izvršavanja skratilo sa približno 18.5 sekundi na 9.5 sekundi